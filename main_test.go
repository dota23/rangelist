package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAdd(t *testing.T) {
	rl := RangeList{}
	err := rl.Add([2]int{1, 5})
	assert.Nil(t, err)
}

func TestPrint(t *testing.T) {
	rl := RangeList{}
	err1 := rl.Add([2]int{1, 5})
	err2 := rl.Print()
	assert.Nil(t, err1)
	assert.Nil(t, err2)
}

func TestRemove(t *testing.T) {
	rl := RangeList{}
	rl.Add([2]int{1, 5})
	rl.Add([2]int{10, 20})
	rl.Add([2]int{20, 20})
	rl.Add([2]int{20, 21})
	rl.Add([2]int{2, 4})
	rl.Add([2]int{3, 8})
	rl.Remove([2]int{10, 10})
	err := rl.Remove([2]int{1, 5})
	rl.Print()
	assert.Nil(t, err)
}

func TestMain(m *testing.M) {
	fmt.Println("begin")
	m.Run()
	fmt.Println("end")
}
