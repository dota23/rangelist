# RangeList

## Requirement
### Task: Implement a struct named 'RangeList'
### A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
### A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
### NOTE: Feel free to add any extra member variables/functions you like.



## Attention
* production-quality code in a team setting
* simple, clean, readable and maintainable
* Space and time complexity
* Code organization and submission format
* mastery of idiomatic Go programming

## Result
![avatar](./result.png)

## TestResult
![avatar](./testResult.png)